### What is this repository for? ###

This is a set of Touchflow modules to use kinect gestures and movements in the gear VR. More detailed, it allows to send gear VR controller movements (up / down / left / right) by kinect movement of for example the user's hand.

### How do I get set up? ###

Just check out the project and start the main class.

### Credits ###

Touchflow and the additional gear modules have been created by:

Frederic raber (frederic.raber@dfki.de)