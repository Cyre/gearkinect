package de.raber.gearkinect;

import de.embots.touchflow.TouchFlow;
import de.embots.touchflow.module.factory.LibraryManager;
import de.raber.gearkinect.modules.GearKinectMod;

public class GearKinect {
	public static void main(String[] args){
		
		//register additional Mod
		LibraryManager.manager.registerModule(new GearKinectMod());
		
		TouchFlow.startGraph("res/gearkinect.xml");
	}

}
