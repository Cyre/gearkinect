package de.raber.gearkinect.modules;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Timer;
import java.util.TimerTask;

import org.jdom.Element;

import de.embots.touchflow.exceptions.ModulException;
import de.embots.touchflow.module.core.OutputModule;
import de.embots.touchflow.module.core.PinName;
import de.embots.touchflow.module.pin.InputPin;
import de.embots.touchflow.module.pin.InputPin3D;

public class GearKinectMod extends OutputModule {

	
	double lastX, lastY;
	Robot robot;
	double divider=0.1;
	
	@Override
	public String getModuleName() {
		// TODO Auto-generated method stub
		return "GearKinect";
	}

	@Override
	protected void processData() throws ModulException {
		double x=getInputPin(PinName.IN).getData();
		double y=getInputPin2D(PinName.IN).getData2();

		if (x!=lastX && y!=lastY){
			double xdiff=lastX-x;
			double ydiff=lastY-y;
			
			if (xdiff<0){
				robot.keyPress(KeyEvent.VK_LEFT);
				scheduleKeyUp(xdiff/divider, KeyEvent.VK_LEFT);
			}
			else{
				robot.keyPress(KeyEvent.VK_RIGHT);
				scheduleKeyUp(xdiff/divider, KeyEvent.VK_RIGHT);
			}
			
			if (ydiff<0){
				robot.keyPress(KeyEvent.VK_DOWN);
				scheduleKeyUp(xdiff/divider, KeyEvent.VK_DOWN);				
			}
			else{
				robot.keyPress(KeyEvent.VK_UP);
				scheduleKeyUp(xdiff/divider, KeyEvent.VK_UP);
			}
		}
		
		

	}

	private void scheduleKeyUp(double offset, int key) {
		Timer t=new Timer();
		t.schedule(new TimerTask() {
			
			@Override
			public void run() {
				robot.keyRelease(key);
				t.cancel();
				
			}
		}, (long)offset);
		
	}

	@Override
	protected void additionalSaveAttribute(Element e) {
		// TODO Auto-generated method stub
		
	}
	
	public GearKinectMod(){
		super();
		inputPins=new InputPin[1];
		
		inputPins[0]=new InputPin3D(PinName.IN, this);
		try {
			robot=new Robot();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
